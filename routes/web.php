<?php

Route::get('/blog/category/{slug?}', 'BlogController@category')->name('category'); //отобраэение категорий
Route::get('/blog/article/{slug?}', 'BlogController@article')->name('article'); // отображение новостей
Route::get('/blog/author/', 'BlogController@author')->name('author'); // отображение авторов

Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
Route::get('/', 'DashboardController@dashboard')->name('admin.index');
Route::resource('/category', 'CategoryController', ['as'=>'admin']);
Route::resource('/article', 'ArticleController', ['as'=>'admin']);
Route::resource('/author', 'AuthorController', ['as'=>'admin']);
Route::resource('/role', 'RoleController', ['as'=>'admin']);
});


Route::group(['prefix'=>'blog'], function(){
    Route::get('/', 'BlogController@review')->name('blog.index');
    Route::resource('/review', 'ReviewController', ['as'=>'blog'] );
    });
    
// Route::post('/blog/review/create', 'ReviewController@create');

Route::get('/', function () {
    return view('blog.home');
});


Auth::routes();

Route::get('/home', 'HomeController@index');
