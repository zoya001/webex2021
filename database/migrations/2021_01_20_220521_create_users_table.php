<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
/**
* Run the migrations.
*
* @return void
*/
public function up()
{
Schema::create('users', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name');
    $table->string('email')->unique();
    $table->string('password');
    $table->string('nickname')->nullable();
    $table->string('information')->nullable();
    $table->string('image')->nullable();

    // $table->integer('role_id')->unsigned()->nullable();

    // ЕСЛИ я буду менять миграции, то попробовать по дефолту создавать польхователей с айдишником категории
    $table->foreign('role_id')->references('id')->on('roles');

    $table->rememberToken();
    $table->timestamps();
});
}

/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
Schema::drop('users');
}
}
