<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $primaryKey='id';
    
    protected $fillable = [
        'short_comment', 'comment', 'user_id'
    ];

    public function authorReview(){
        return $this->belongsTo('App\User', 'user_id');
      }
}
