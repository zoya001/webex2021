<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{

    protected $primaryKey='id';

     protected $fillable = ['title', 'shortDescription',  'description', 'slug', 
     'category_id', 'meta_description', 'keyword', 'meta_title', 'image', 'user_id',
    ];

     public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug( mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmyHi'), 
        '-');
      }

      public function category(){
        return $this->belongsTo('App\Category', 'category_id');
      }

      public function author(){
        return $this->belongsTo('App\User', 'user_id');
      }
}
