<?php

namespace App\Http\Controllers;

use App\Review;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::paginate(3);
		return view('blog.reviews.index', ['reviews' => $reviews]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('blog.reviews.create',['users'=> $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'short_review'=>'required',
            'review_text',
            'user_id'=>'required',
            ]);

            $review = new Review;
            $review->short_review = $request->short_review;
            $review->review_text = $request->review_text;
            $review->user_id = $request->user_id;
        
            $review->save();
    
            return redirect()->route('blog.review.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
        $review = Review::find($id);
        $users = User::all()->where('id', '<>', $review->user_id);
		return view('blog.reviews.edit', ['users' => $users, 'review' => $review]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'short_review'=>'required',
            'review_text',
            'user_id',
            ]);

            $review = Review::find($id);
            $review->short_review = $request->short_review;
            $review->review_text = $request->review_text;
            $review->user_id = $request->user_id;
            $review->save();
            return redirect()->route('blog.review.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::find($id);
        
        $review->delete();
        return redirect()->route('blog.review.index');
    }
}
