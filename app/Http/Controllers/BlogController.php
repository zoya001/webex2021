<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Article;
use App\User;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function category($slug){
        $category=Category::where('slug', $slug)->firstOrFail();
        return view('blog.category', [
            'category'=> $category,
            'articles'=> $category->article()->paginate(8),
        ]);
    }

   /* public function article($slug) {
        $article=Article::where('slug', $slug)->firstOrFail();
    	return view('blog.article', [
    		'article' => $article->paginate(8),
    	]);
    }*/
    public function article($slug) {
    	return view('blog.article', [
    		'article' => Article::where('slug', $slug)->firstOrFail()
    	]);
    }

    // public function author() {
    // 	return view('blog.author', [
    // 		'user' => User::firstOrFail()
    // 	]);
    // }

    public function author() {
		
		$users = User::all();
			
		return view('blog.author', ['users' => $users]);
		
    }
    
    public function review() {
        return view('blog', [
        
    ]);
        }
    // public function returnViewAuthor() {
    //     return View::make('view')->with('author',$information);
    // }
    
}
