<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ValidatesRequests;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
		$users = User :: paginate(4);

		return view('admin.authors.index', ['users' => $users]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles=Role::all();
        return view('admin.authors.create',['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'nickname'=>'required',
            'information',
            'email'=>'required',
            'image',
            'password'=>'required',
            'role_id'
            ]);

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->nickname = $request->nickname;
            $user->information = $request->information;
            $user->image = $request->image;
            $user->role_id = $request->role_id;
            $user->password = bcrypt($request->password);

            $user->save();

            return redirect()->route('admin.author.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        $roles = Role::all()->where('id', '<>', $user->role_id);
        
		return view('admin.authors.edit', [ 'roles' => $roles, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        
        $this->validate($request, [
            'name'=>'required',
            'nickname'=>'required',
            'information',
            'email'=>'required',
            'image',
            'role_id'=>'required',
            ]);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->nickname = $request->nickname;
            $user->information = $request->information;
            $user->image = $request->image;
            $user->role_id = $request->role_id;
            
            $user->save();
            return redirect()->route('admin.author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {       
        $user = User::find($id);
        
        foreach ($user->review as $review){
            $review->delete();
        }

        foreach ($user->articleAuthor as $article){
            $article->user_id= NULL;
            $article->save();
        }

        $user->delete();
        return redirect()->route('admin.author.index');
    }
}
