<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::paginate(6);
        
        return view('admin.articles.index', ['articles' => $articles]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Category::all();
        $users = User::all();
        return view('admin.articles.create', ['categories' => $categories, 'users'=> $users]);
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'category_id' => 'required',
            'title' => 'required',
            'shortDescription',
            'description'=> 'required',
            'meta_title',
            'meta_description',
            'keyword',
            'slug' => 'readonly',
            'image',
            'user_id'=>'required',
		]);
		
            $article = new Article;
    
            $article->title = $request->title;
            $article->shortDescription = $request->shortDescription;
            $article->description = $request->description;
            $article->meta_description = $request->meta_description;
            $article->meta_title = $request->meta_title;
            $article->keyword = $request->keyword;
            $article->slug = $request->slug;
            $article->image = $request->image;
            $article->user_id = $request->user_id;
            $article->category_id = $request->category_id; 

            $article->save();
            return redirect()->route('admin.article.index');
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
		return view('admin.articles.show', ['article' => $article]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function  edit($id)
    {
    
		$article = Article::find($id);	
        $categories = Category::all()->where('id', '<>', $article->category_id);
        $users = User::all()->where('id', '<>', $article->user_id);
        
        return view('admin.articles.edit', ['categories' => $categories,  'users'=> $users, 'article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $this->validate($request, [
            'category_id'=>'required',
            'title',
            'shortDescription',
            'description',
            'meta_title',
            'meta_description',
            'keyword',
            'image',
            'slug',
            // 'created_by'=>'required',
		]);
				
		$article = Article::find($id);
		
        $article->category_id = $request->category_id;
        $article->title = $request->title;
        $article->shortDescription = $request->shortDescription;
        $article->description = $request->description;
        $article->meta_title = $request->meta_title;
        $article->keyword = $request->keyword;
        $article->meta_description = $request->meta_description;
        $article->image = $request->image;
        // $article->created_by = $request->created_by;

		$article->save();
		return redirect()->route('admin.article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
		$article->delete();
		return redirect()->route('admin.article.index');
    }
}
