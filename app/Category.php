<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{

    //parent не задействован
    protected $table='categories';
    protected $primaryKey='id';
    protected $fillable=['title', 'slug', 'created_by','updated_by', 'category_id']; 

    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug( mb_substr($this->title, 0, 40) . "-" . \Carbon\Carbon::now()->format('dmyHi'), 
        '-');
      }

      //категория - главная. одна категория - много статей.

      public function article(){
        return $this->hasMany('App\Article');
      }

}
