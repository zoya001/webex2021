@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Поздравляем!</div>

                <div class="panel-body">
                   Вы успешно прошли авторизацию!
                </div>
            </div>
            
<div class="pull-left" style="padding-right:20px">
                <a href="http://webexam.std-1219.ist.mospolytech.ru/" class="btn btn-primary">Перейти на главную страницу</a>
            </div>
        </div>
        
    </div>
</div>

@endsection


