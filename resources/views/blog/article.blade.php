@extends('layouts.app')


@section('title', $article->meta_title)
@section('keyword', $article->keyword)
@section('meta_description', $article->meta_description)

@section('content')

<!doctype html>
<html lang="en">
  <head>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/blog/">
<link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
<meta name="theme-color" content="#7952b3">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      img {
        max-width: 100%;
    }
    </style>

    <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
    <link href="blog.css" rel="stylesheet">
  </head>
  <body>

  <main class="container">
  <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
    <div class="col-md-12 px-0" style="background-color: #1f1f23; border-radius: 10px; border: 4px double black; padding:4px; margin:10px">
      <h1 class="display-4 font-italic" style="color:white;  padding:2px; margin:1px">{{$article->title or ""}}</h1>
      <p class="lead my-3"  style="color:white; padding:4px; margin:10px">{{$article->meta_description or ""}}</p>
      <p class="lead mb-0"><a href="#reading" class="text-white fw-bold" style="padding:4px; margin:10px">Продолжить чтение...</a></p>
    </div>
  </div>
<br>
<main class="container">
  <div class="row">
    <div class="col-md-8">
        <br>
        <article class="blog-post" id="reading">
        <h2 class="blog-post-title">{{$article->title or ""}}</h2>
        <p class="blog-post-meta">{{$article->created_at or ""}}
        
      @if($article->user_id == NULL)
      Автор был удален.
        @else 
        , {{$article->author->nickname or ""}}.
        @endif
       
        </p>
        <h3>Категория: {{$article->category->title or ""}}</h3>
        <hr>
        <h3>Ключевые слова: {{$article->keyword or ""}}</h3>
        <p>{{$article->meta_title or ""}}. {{$article->meta_description or ""}}.</p>
        <p>{{$article->shortDescription or ""}}</p>
        <hr>
        <p> {{$article->description or ""}}</p>
        <hr>
        @if($article->image)
        <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="100%" height="100%" src="{{$article->image or ""}}" alt="Вложение" preserveAspectRatio="xMidYMid slice" focusable="false" role="image" aria-label="Placeholder: 500x500"></img> 
        
            <hr>
        @else 
        <br>
        @endif

        <input type="button" class="btn btn-primary" style="background-color:black; border-color:black;" onclick="history.back();" value="Назад"/>
        <a href="/" class="btn btn-primary" style="background-color:black; border-color:black;"> На главуню</a>
        <!-- <a href="" class="btn btn-secondary">Перейти к комментариям...</a> -->
    </div>

    <div class="col-md-4">
      <div class="p-4 mb-3 bg-light rounded">
      <br>
      <br>

      @if($article->user_id == NULL)
      <h4 class="font-italic">Автор был удален.</h4>
        @else 
        <h4 class="font-italic">Автор</h4>
        <p class="mb-0">{{$article->author->name or ""}}</p>
        @endif

      </div>
  </div>
</main>

  </body>
</html>

@endsection