@extends('layouts.app')
@section('title', $category->title)

@section('content')
<div class="container">

    @forelse ($articles as $article)
    <div class="row">
        <div class="col-sm-12">
            <h1><a href="{{route('article', $article->slug)}}">{{$article->title}}</a></h1>
            <p>{!!$article->shortDescription!!}</p>
        </div>
    </div>
    @empty
    <h1 class="text-center">Пусто</h1>
    @endforelse

    {{$articles->links()}}
    <br>
    <a href="/" class="btn btn-primary">На главуню</a>
    
</div>


@endsection