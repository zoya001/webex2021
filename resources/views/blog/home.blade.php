@extends('layouts.app')
@section('content')

<!doctype html>
<html lang="ru">
  <head>
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Пример на bootstrap 4: Макет jumbotron с навигационной панели и базовая система разметки. Версия v4.4.1.">

    <title>Блог</title>

<link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
    
    body{
      background:#fcfcfc;
    }

      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      

   .jumbotron {
    background: #c7b39b url(http://server.std-1219.ist.mospolytech.ru/unnamed_home.jpg);

    color: #fff;
    margin:auto;
    padding:auto;

    width: 100%;
    background-size: 100%;
   }

  .contacts{
    padding:10px;
    margin:10px;
    background: white;
 
  }
  
  .marketing{
    
  }

  img {
    max-width: 100%;
}
  
  </style>

    <link href="jumbotron.css" rel="stylesheet">
  </head>
  <body>



<script defer>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4481610-59', 'auto');
  ga('send', 'pageview');

</script>

<script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(39705265, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/39705265" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-8588635311388465",
    enable_page_level_ads: true
  });
</script>

<main role="main">

  <div class="jumbotron">
    <div class="container">  
      <h1 class="display-3">Добро пожаловать!</h1>
      <p style="color:">Вы попали на страницу блога. На данном сайте вы можете ознакомиться со статьями различных авторов, ведущих данный блог! Писатели поднимают различные темы, начиная от экологии, закакнчивая политикой и углубляясь в философию.</p>
      <p><a class="btn btn-primary btn-lg" href="#contacts" role="button">Оформите подписку прямо сейчас! &raquo;</a></p>
        </div>
  </div>

<hr>

  <div class="container" id="contacts">

    <div class="contacts">
    <div class="row">
      <div class="col-md-4">
        <h2>Мы в Instagram</h2>
        <p>Больше эксклюзивного контента, сопровождаемое уникальным оформлением, только тут! Следите за обновлениями блога с помощью историй Instagram! </p>
        <p><a class="btn btn-secondary" href="https://www.instagram.com/q.aatdb" role="button">Подписаться &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Мы в Telegram</h2>
        <p>Хотите услышать больше историй? Переходите по ссылке прямо сейчас! На нашем канале вы найдете еще больше интересного и увлекательного контента! </p>
        <p><a class="btn btn-secondary" href="https://web.telegram.org/" role="button">Подписаться &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Мы в VK</h2>
        <p>Вы можете найти нас также в популярнейшей соц. сети СНГ! Подпишитесь на нашу страницу Вконтакте, чтобы следить за всеми новостями!</p>
        <p><a class="btn btn-secondary" href="https://vk.com/qaatdb" role="button">Подписаться &raquo;</a></p>
      </div>
    </div>
</div>
</div>

  <div class="container marketing">
  <hr>
<div class="row"  style="background:white;" >
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body" style="text-align:center;">
        <h5 class="card-title">Авторы статей</h5>
        <p class="card-text">Кто ведет данный блог? Ознакомьтесь с нашими авторами по ссылке снизу!</p>
        <a href="/blog/author" class="btn btn-primary">Перейти к просмотру</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body" style="text-align:center;">
        <h5 class="card-title">Отзывы посетителей блога</h5>
        <p class="card-text">Интересно, что думают о сайте другие? Не секрет!</p>
        <a href="{{route('blog.review.store')}}" class="btn btn-primary" >Перейти к просмотру</a>
      </div>
    </div>
  </div>
</div>

    <div class="marketing">
    <hr class="featurette-divider">

      <div class="row featurette" style="background:white;">
      <div class="col-md-5">
      <img  class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="400" height="400" src="http://server.std-1219.ist.mospolytech.ru/first.png" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><rect width="100%" height="100%" fill="#eee"/><text x="50%" y="50%" fill="#aaa" dy=".3em"></text>
    </div>
    <div class="col-md-7">
      <h2 class="featurette-heading">Почему стоит читать нас?</h2>
      <p class="lead">Уникальные истории только для посетителей этого сайта. Здесь Вы встретите контент, который прежде нигде не выкладывался.</p>
    </div>
   
  </div>

    <hr class="featurette-divider">

    <div class="row featurette" style="background:white;">
  <div class="col-md-7 order-md-2" >
    <h2 class="featurette-heading">Неужели стоит подписаться на данный блог?</h2>
    <p class="lead">Задумайтесь о всех плюсах, что даст Вам подписка на наши страницы в социальных сетях. Вы найдете здесь интересные истории, шокирующие факты, а также столкнетесь с увлекательным интерактивом. </p>
  </div>
  <div class="col-md-5 order-md-1">
    <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="400" height="400" src="http://server.std-1219.ist.mospolytech.ru/third.png" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><rect width="100%" height="100%" fill="#eee"/><text x="50%" y="50%" fill="#aaa" dy=".3em"></text>
  </div>
</div>


    <hr class="featurette-divider">


    <div class="row featurette" style="background:white;">
    <div class="col-md-5">
    <img src="http://server.std-1219.ist.mospolytech.ru/second.png" class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="400" height="400"  preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><rect width="100%" height="100%" fill="#eee"/><text x="50%" y="50%" fill="#aaa" dy=".3em"></text>
  </div>
    <div class="col-md-7">
    <h2 class="featurette-heading">Оставайтесь с нами на связи! <span class="text-muted">В любое время.</span></h2>
    <p class="lead">Авторы блога находятся всегда на связи, так что Вы можете задать вовпрос в любое время и получить на него гарантированный ответ. А может, Вы желаете присоединиться к нашей команде? :)</p>
  </div>
</div>

</div>

    <hr class="featurette-divider">
    <footer class="container" >
    <p class="float-right"><a href="#">Наверх</a></p>
    <p>&copy; 2020-2021 Московский Политех. &middot; <a href="#">Политика конфиденциальности</a> &middot; <a href="#">Условия обслуживания</a></p>
  </footer>
  </main>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
      </body>
</html>
@endsection