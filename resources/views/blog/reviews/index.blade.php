@extends('layouts.app')
@section('content')

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
<link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

<link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">

<script src="https://code.jquery.com/jquery-git.min.js"></script>

<meta name="theme-color" content="#7952b3">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      
      body{
      background:#fcfcfc;
    }

    #upbutton {
    background: url("/images/up.png") no-repeat top left;
    height: 60px;
    width: 60px;
    bottom: 30px;
    right: 30px;
    cursor: pointer;
    display: none;
    position: fixed;
    z-index: 999;
  }

      img {
        max-width: 100%;
    }

    </style>
  </head>
  <body>
<main>
<section class="jumbotron text-center" style="background: url(http://server.std-1219.ist.mospolytech.ru/unnamed.jpg) repeat-y;  margin:auto; padding:auto; width: 100%; background-size: 100% auto;">
    <div class="container">
      <h1 class="jumbotron-heading" style="color: RGB(47, 123, 184); text-shadow: white 0 0 2px; border-radius: 15px; ">Отзывы о нашем сайте</h1>
      <p class="lead text-muted" style="color:black;  text-shadow: white 0 0 2px; line-height:160%;  border-radius: 10px; ">Предлагаем ознакомиться с отзывами наших читателей!</p>
      <p>
        <a href="/" class="btn btn-primary my-2" style="margin:10px;">Вернуться на главную</a>
      </p>
    </div>
  </section>

  @if(\Auth::check())
  
  <h1 style="text-align:center;">Добавьте свой отзыв!</h1>
<div class="container" >

<hr>

<form class="form-horizontal" action="{{route('blog.review.store')}}" method="post">
{{ csrf_field() }}

@include('blog.reviews.part.create_form')
<input type="hidden" name="user_id" value="{{Auth::id()}}">

</form>
</div>
  @endif
  </div>

<br>


  <div class="container">
  @if(!(\Auth::check()))
  <hr>
<div>
<b class = "text-danger">Авторизуйтесь, чтобы добавить отзыв!</b>
</div>
<hr>

  @endif
  </div>
  @foreach ($reviews as $review)
  <div class="container">
  <div class="card text-center" style="border-radius: 15px; background-color:#ebebeb; padding: 4px" >
  <div class="card-header">
  <h3>
    Автор отзыва: {{$review->authorReview->name or ""}}
    </h3>
    <hr>
  </div>
  <div class="card-body">
  <h3 class="card-title"> {{$review->short_review}}</h3>
  @if($review->review_text)
    <h4 class="card-text">Отзыв: {{$review->review_text}}</h4>
    <hr>
    <p>Спасибо за отзыв!</p> 
    @else
    <hr>
    <p>Спасибо за отзыв!</p> 
    @endif
    
    @if((\Auth::check()))

    @if(($user=Auth::user()->id == $review->user_id) or (auth()->user()->role_id == 2))
    <form onsubmit="if (confirm ('Удалить?') ) 
    { return true } 
    else 
    {return false}
    " action="{{route('blog.review.destroy', $review)}}" method="post">
    <input type="hidden" name="_method" value="DELETE">
    {{ csrf_field() }}
    <div style="padding:1px; margin:1px;">
          <button type="submit" class="btn btn-primary">Удаление</button>
     </div>
    @endif

    @endif

  </div>
  <div class="card-footer text-muted">
  {{$review->created_at or ""}}
  </div>
</div>
</div>
<br>
@endforeach

@forelse ($reviews as $review)
@empty
  <h1 class="text-center">Пока тут пусто! Мы будем рады Вашим отзывам!</h1>
  @endforelse
  
  <div class="container" style="text-align:center;">
<ul class="pagination pull-center">
{{$reviews->links()}}
</ul>
</div>

</main>
<hr class="featurette-divider">
<footer class="container">
    <p class="float-right"><a href="#">Наверх</a></p>
    <p>&copy; 2020-2021 Московский Политех. &middot; <a href="#">Политика конфиденциальности</a> &middot; <a href="#">Условия обслуживания</a></p>
  </footer>
    <script src="/docs/5.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  </body>
</html>
@endsection