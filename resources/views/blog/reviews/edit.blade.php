@extends('layouts.app')

@section('content')
@if($user=Auth::user()->id == $review->user_id)
<h1 style="text-align:center;">Редактирование</h1>
<div class="container">

<hr>

<form class="form-horizontal" action="{{route('blog.review.update', $review)}}" method="post">
<input type="hidden" name="_method" value="PUT">
{{ csrf_field() }}

    @include('blog.reviews.part.create_form')
    <div class="pull-left" style="padding-right:20px">
                <a href="{{route('blog.review.store')}}" class="btn btn-primary">Назад</a>
            </div>
</form>
</div>

@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection