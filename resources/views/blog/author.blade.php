@extends('layouts.app')
@section('content')

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
<link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

<link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">

<script src="https://code.jquery.com/jquery-git.min.js"></script>

<meta name="theme-color" content="#7952b3">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      
      body{
      background:#fcfcfc;
    }

    #upbutton {
    background: url("/images/up.png") no-repeat top left;
    height: 60px;
    width: 60px;
    bottom: 30px;
    right: 30px;
    cursor: pointer;
    display: none;
    position: fixed;
    z-index: 999;
  }

      img {
        max-width: 100%;
    }
    </style>
  </head>
  <body>
<main>
<section class="jumbotron text-center" style="background: url(http://server.std-1219.ist.mospolytech.ru/bg3.jpg) repeat-y;  margin:auto; padding:auto; width: 100%; background-size: 100% auto;">
    <div class="container">
      <h1 class="jumbotron-heading" style="color:white; background-color: RGB(47, 123, 184, 0.4);  border-radius: 15px; ">Авторы статей</h1>
      <p class="lead text-muted" style="color:white; background-color: RGB(47, 123, 184, 0.6); line-height:160%;  border-radius: 10px; ">Предлагаем ознакомиться с нашими авторами! На данной странице Вам представляется возможность узнать немного интересного о тех, кто радует читателей своими материалами.</p>
      <p>
        <a href="/" class="btn btn-primary my-2" style="margin:10px;">Вернуться на главную</a>
        <a href="/#contacts" class="btn btn-primary my-2 ">Перейти к контактам</a>
      </p>
    </div>
  </section>
  
  <br>
  <br>
 
<a id="upbutton" href="#" onclick="smoothJumpUp(); return false;">
    <img src="http://server.std-1219.ist.mospolytech.ru/up2.png" alt="Top" border="none" title="Наверх">
</a>

<script>
    var smoothJumpUp = function() {
        if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
            window.scrollBy(0,-50);
            setTimeout(smoothJumpUp, 10);
        }
    }
    
    window.onscroll = function() {
      var scrolled = window.pageYOffset || document.documentElement.scrollTop;
      if (scrolled > 100) {
      	document.getElementById('upbutton').style.display = 'block';
      } else {
      	document.getElementById('upbutton').style.display = 'block';
      }
    }
</script>


  <div class="container">

@foreach ($users as $user)
@if($user->role_id == 3)
<div class="row featurette" style="">
  <div class="col-md-8">
  <br>
    <h2 class="featurette-heading">Автор: {{$user->name}}</h2>
    <p class="lead"><span class="text-muted"> 
    Никнейм: {{$user->nickname}}</span></p>
    @if($user->information)
    <p class="lead"> <br>Информация об авторе: {{$user->information}}</p>
    @else
    <p class="lead"> <br>Информация об авторе недоступна.</p>
    @endif
  </div>
  <div class="image">
  <div class="col-md-4">
  @if($user->image)
  <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="380" height="380" src="{{$user->image}}" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><rect width="100%" height="100%" fill="#eee"/><text x="50%" y="50%" fill="#aaa" dy=".3em"></text></img>
    @else 
    <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" width="380" height="380" src="http://server.std-1219.ist.mospolytech.ru/author.jpg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 500x500"><rect width="100%" height="100%" fill="#eee"/><text x="50%" y="50%" fill="#aaa" dy=".3em"></text></img>
    @endif
  </div>
  </div>
  </div>

  <hr class="featurette-divider">
  @endif
    @endforeach

    @forelse ($users as $user)

@empty
  <h1 class="text-center">Пусто</h1>
  @endforelse
  

</div>

<hr class="featurette-divider">

<footer class="container">
    <p class="float-right"><a href="#">Наверх</a></p>
    <p>&copy; 2020-2021 Московский Политех. &middot; <a href="#">Политика конфиденциальности</a> &middot; <a href="#">Условия обслуживания</a></p>
  </footer>
</main>
    <script src="/docs/5.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
  </body>
</html>
@endsection