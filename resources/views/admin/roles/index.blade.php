@extends('admin.layouts.app_admin')

@section('content')
@if(auth()->user()->role_id == 2)
<h1 style="text-align:center;">Добавление ролей</h1>
<div class="container" >

<a href="{{route('admin.role.create')}}" class="btn btn-primary pull-right">
    <i class="">Добавить роль</a>
    <table class="table table-striped">
        <thead>
        <th>ID</th>
            <th>Наименование</th>
            <th>Дата добавления</th>
            <th class="text-right">Действие</th>
</thead>
<tbody>
    @forelse ($roles as $role)
<tr>
<td> 
        {{$role->id}}
    </td>
    <td> 
        {{$role->role_name}}
    </td>
    <td> 
        {{$role->created_at}}
    </td>
    <td> 

    <div class="pull-right" style="">
          <a href="{{route('admin.role.edit', $role)}}" class="btn btn-primary">Редактирование</a>
     </div>

    <form onsubmit="if (confirm ('Удалить?') ) 
    { return true } 
    else 
    {return false}
    " action="{{route('admin.role.destroy', $role)}}" method="post">
    <input type="hidden" name="_method" value="DELETE">
    {{ csrf_field() }}

    <div class="pull-right" style="padding-right:10px;">
          <button type="submit" class="btn btn-primary">Удаление</button>
     </div>


    </form>
     
     
    </td>
</tr>
    @empty
<tr>
    <td colspan="4" class="text-center">
<h1>
    Данные отсутствуют
</h1>
</td>
</tr>
    @endforelse
</tbody>

<tfoot>
<tr>
<td colspan="4" class="text-center">
<ul class="pagination pull-center">
{{$roles->links()}}
</ul>
</td>
</tr>
</tfoot>
</table>
</a>

<div class="pull-left" style="padding-right:20px">
                <a href="/admin" class="btn btn-primary">Назад</a>
            </div>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection