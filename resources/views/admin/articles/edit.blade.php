@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or ((auth()->user()->role_id == 3) and ($user=Auth::user()->id == $article->user_id)))
<div class="container">
<h1 style="text-align:center;">Редактирование статьи</h1>
<hr>
<div>
<b class = "text-danger">  При редактировании статьи заполните поле "категория"</b>
</div>
<hr>


<form class="form-horizontal" action="{{route('admin.article.update', $article)}}" method="post">
<input type="hidden" name="_method" value="PUT">
{{ csrf_field() }}

@include('admin.articles.part.edit_art_form')

<!-- <input type="hidden" name="updated_by" value="{{Auth::id()}}"> -->

</form>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection