<label for="">Заголовок</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок новости" value="{{$article->title or ""}}" required>

<label for="">Категория</label>
<form action="{{route('admin.article.edit', $article)}}" method="POST" class="form-horizontal mt-3">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
							<select class = "form-control" name = "category_id">
								@if ($article->category)
									<option value = "{{$article->category_id }}">{{ $article->category->title }}</option>
								@else
									<option>Не выбрано</option>
								@endif
								@foreach ($categories as $category)
									<option value = "{{$category->id or ""}}" required> {{ $category->title }} </option>
								@endforeach
							</select>                 
<hr>
<label for="">Краткое описание</label>
<textarea class="form-control" id="shortDescription" name="shortDescription" >{{$article->shortDescription or ""}}</textarea>

<label for="">Полное описание</label>
<textarea class="form-control" id="description" name="description" required>{{$article->description or ""}}</textarea>
<hr />
<label for="">Мета заголовок</label>
<input type="text" class="form-control" name="meta_title" placeholder="Мета заголовок" value="{{$article->meta_title or ""}}">

<label for="">Мета описание</label>
<input type="text" class="form-control" name="meta_description" placeholder="Мета описание" value="{{$article->meta_description or ""}}">

<label for="">Ключевые слова</label>
<input type="text" class="form-control" name="keyword" placeholder="Ключевые слова, через запятую" value="{{$article->keyword or ""}}">

<label for="">Изображение к статье</label>
<input type="text" class="form-control" name="image" placeholder="Ссылка на изображение к статье" value="{{$article->image or ""}}">
<hr>
<input class="btn btn-primary" type="submit" value="Сохранить">

<div class="pull-left" style="padding-right:20px">
                <a href="{{route('admin.article.store')}}" class="btn btn-primary">Назад</a>
            </div>

</div>