@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<h1 style="text-align:center;">Создание статей</h1>
<div class="container" >

<a href="{{route('admin.article.create')}}" class="btn btn-primary pull-right">
    <i class="">Создать статью</a>
    <table class="table table-striped">
        <thead>
            <th>Название статьи</th>
            <th>Категория</th>
            <th>Автор статьи</th>
            <th>Публикация</th>
            
            <th class="text-right">Действие</th>
</thead>
<tbody>
    @forelse ($articles as $article)
<tr>
    <td> 
        {{$article->title}}
    </td>
    <td> 
        {{$article->category->title or ""}}
    </td>
    <td> 
         {{$article->author->nickname or ""}}
    </td>
    <td> 
        {{$article->created_at}}
    </td>
    <td> 
    <div class="pull-right" style="padding:1px; margin:1px;">
          <a href="{{route('admin.article.show', $article)}}" class="btn btn-primary">Просмотр</a>
     </div>

    @if(($user=Auth::user()->id == $article->user_id) or (auth()->user()->role_id == 2))
    <div class="pull-right" style="padding:1px; margin:1px;">
          <a href="{{route('admin.article.edit', $article)}}" class="btn btn-primary">Редактирование</a>
     </div>
  
 
    <form onsubmit="if (confirm ('Удалить?') ) 
    { return true } 
    else 
    {return false}
    " action="{{route('admin.article.destroy', $article)}}" method="post">
    <input type="hidden" name="_method" value="DELETE">
    {{ csrf_field() }}

    <div class="pull-right" style="padding:1px; margin:1px;">
          <button type="submit" class="btn btn-primary">Удаление</button>
     </div>


    </form>
     
     @endif
    </td>
</tr>
    @empty
<tr>
    <td colspan="5" class="text-center">
<h1>
    Данные отсутствуют
</h1>
</td>
</tr>
    @endforelse
</tbody>

<tfoot>
<tr>
<td colspan="5" class="text-center">
<ul class="pagination pull-center">
{{$articles->links()}}
</ul>
</td>
</tr>
</tfoot>

</table>
</a>

<div class="pull-left" style="padding-right:20px">
                <a href="/admin" class="btn btn-primary">Назад</a>
            </div>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection