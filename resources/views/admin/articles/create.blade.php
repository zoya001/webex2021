@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<h1 style="text-align:center;">Создание статьи</h1>
<div class="container">
<hr>
<div>
<b class = "text-danger">  При написании статьи заполните поле "категория"</b>
</div>
<hr>

<form class="form-horizontal" action="{{ route('admin.article.store') }}" method="POST">
{{ csrf_field() }}

    @include('admin.articles.part.form')

<input type="hidden" name="user_id" value="{{Auth::id()}}">
</form>
</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection
