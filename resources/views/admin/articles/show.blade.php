@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>{{$article->title}}</h1>
            <h3>Категория статьи: {{$article->category->title or ""}}</h3>
            <br>
            <p>{{$article->meta_title or ""}}</p>
            <p>{{$article->meta_description or ""}}</p>
            @if($article->keyword)
            <p>Ключевые слова: {{$article->keyword}}.</p>
            @else
            <p> Не нашлось подходящих ключевых слов.</p>
            @endif
            <br>
            <p>{{$article->shortDescription}}</p>
            <p>{{$article->description}}</p>
            @if($article->image)
            <img width="100%" height="100%" src="{{$article->image or ""}}" alt="Вложение"></img>   
            <br>      
                @else 
                <br>
                @endif
            <h4 class="font-italic">Кем подготовлена статья:
              @if($article->user_id == NULL)
                автор был удален.
                    @else 
                         {{$article->author->name or ""}}
                    @endif
             </h4>
            <a href="{{route('admin.article.store')}}" class="btn btn-primary">Назад</a>
        </div>
    </div>
</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection