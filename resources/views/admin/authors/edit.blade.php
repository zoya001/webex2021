@extends('admin.layouts.app_admin')

@section('content')
@if(auth()->user()->role_id == 2)
<div class="container">
<h1 style="text-align:center;">Редактирование информации о пользователе</h1>
<hr>

<form class="form-horizontal" action="{{route('admin.author.update', $user)}}" method="post">
<input type="hidden" name="_method" value="PUT">
{{ csrf_field() }}

@include('admin.authors.part.form')

</form>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection
