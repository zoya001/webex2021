@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<h1 style="text-align:center;">Добавление пользователей</h1>
<div class="container" >

<hr>
<div>
<b class = "text-danger"> На данной странице осуществляется администрирование учетными записями пользователей.</b>
</div>
<hr>
@if(auth()->user()->role_id == 2)
<a href="{{route('admin.author.create')}}" class="btn btn-primary pull-right">
    <i class="">Добавить пользователя</a>
@endif
    <table class="table table-striped">
        <thead>
            <th>Автор</th>
            <th>Никнейм</th>
            <th>Уровень доступа</th>
            <th>Адрес электронной почты</th>
            <th>Информация об авторе</th>
               @if(auth()->user()->role_id == 2)
            <th class="text-right">Действие</th>
            @endif
</thead>
<tbody>
    @forelse ($users as $user)
<tr>
    <td> 
        {{$user->name}}
    </td>
    <td> 
        {{$user->nickname}}
    </td>
    
    <td> 
        {{$user->role->role_name or ""}}
    </td>

    <td> 
        {{$user->email}}
    </td>
    <td> 
        {{$user->information}}
    </td>
    <td> 
    
    @if(auth()->user()->role_id == 2)
    <div class="pull-right" style="padding:1px; margin:1px;">
          <a href="{{route('admin.author.edit', $user)}}" class="btn btn-primary">Редактирование</a>
     </div>
    <form onsubmit="if (confirm ('Удалить?') ) 
    { return true } 
    else 
    {return false}
    " action="{{route('admin.author.destroy', $user)}}" method="post">

    <input type="hidden" name="_method" value="DELETE">
    {{ csrf_field() }}

    <div class="pull-right" style="padding:1px; margin:1px;">
          <button type="submit" class="btn btn-primary">Удаление</button>
     </div>
    </form>
    @endif
    </td>
</tr>
    @empty
<tr>
    <td colspan="6" class="text-center">
<h1>
    Данные отсутствуют
</h1>
</td>
</tr>
    @endforelse
</tbody>

<tfoot>
<tr>
<td colspan="6" class="text-center">
<ul class="pagination pull-center">
{{$users->links()}}
</ul>
</td>
</tr>
</tfoot>
</table>
</a>

<div class="pull-left" style="padding-right:20px">
                <a href="/admin" class="btn btn-primary">Назад</a>
            </div>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection