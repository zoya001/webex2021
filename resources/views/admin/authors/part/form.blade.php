
<label for="author-name">Имя автора</label>
<input type="text" class="form-control" name="name" placeholder="Ф.И.О."  value="{{ $user->name or ""}}" class="form-control" id="author-name" required>

<label for="author-nickname">Никнейм</label>
<input type="text" class="form-control" name="nickname" placeholder="Никнейм"  value="{{ $user->nickname or ""}}" class="form-control" id="author-nickname" required>
   
<label for="">Уровень доступа</label>
<form action="{{route('admin.author.edit', $user)}}" method="POST" class="form-horizontal mt-3">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
							<select class = "form-control" name = "role_id">
								@if ($user->role)
									<option value = "{{ $user->role_id }}"> {{ $user->role->role_name }}</option>
								@else
									<option>Не выбрано</option>
								@endif
								@foreach ($roles as $role)
									<option value = "{{$role->id}}">{{ $role->role_name }} </option>
								@endforeach
							</select>
                            
<label for="">Информация об авторе</label>
<input class="form-control" type="text" name="information" placeholder="Дополнительная информация" value="{{ $user->information or "" }}" id="author-information">

<label for="author-email">Адрес электронной почты</label>
<input type="text" class="form-control" name="email" placeholder="email"  value="{{ $user->email or ""}}" class="form-control" id="author-email" readonly="">

<label for="">Изображение</label>
<input class="form-control" type="text" name="image" placeholder="Ссылка на фото профиля автора" value="{{ $user->image or "" }}" id="author-image">

<hr>

<button class="btn btn-primary" type="submit" value="Сохранить">Сохранить</button>

<div class="pull-left" style="padding-right:20px">
                <a href="{{route('admin.author.store')}}" class="btn btn-primary">Назад</a>
            </div>
</div>