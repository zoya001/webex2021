
<label for="author-name">Имя автора</label>
<input type="text" class="form-control" name="name" placeholder="Ф.И.О."  value="{{ $user->name or ""}}" class="form-control" id="author-name" required>

<label for="author-nickname">Никнейм</label>
<input type="text" class="form-control" name="nickname" placeholder="Никнейм"  value="{{ $user->nickname or ""}}" class="form-control" id="author-nickname" required>
   
<label for="">Уровень доступа</label>
<form action="{{ route('admin.author.store') }}" method="POST" class="form-horizontal mt-3">
				{{ csrf_field() }}

        <select class = "form-control" name = "role_id" required>
		<option></option>
								@foreach ($roles as $role)
									<option value = "{{$role->id or ""}}" > {{ $role->role_name }} </option>
								@endforeach
							</select>
                            
<label for="author-information">Информация об авторе</label>
<input class="form-control" type="text" name="information" placeholder="Дополнительная информация" value="{{ $user->information or "" }}" id="author-information">

<label for="author-email">Адрес электронной почты</label>
<input type="text" class="form-control" name="email" placeholder="email"  value="{{ $user->email or ""}}" class="form-control" id="author-email" required>

<label for="">Изображение</label>
<input class="form-control" type="text" name="image" placeholder="Ссылка на фото профиля автора" value="{{ $user->image or "" }}" id="author-image">

<label for="password">Пароль</label>
<input class="form-control" type="password" name="password" placeholder="Пароль" value="{{ $user->password or "" }}" id="author-password" required>

<label for="password">Подтверждение пароля</label>
<input class="form-control" type="password" name="password-conf" placeholder="Подтверждение пароля" required>

<hr>

<button class="btn btn-primary" type="submit" value="Сохранить">Сохранить</button>

<div class="pull-left" style="padding-right:20px">
                <a href="{{route('admin.author.store')}}" class="btn btn-primary">Назад</a>
            </div>
</div>