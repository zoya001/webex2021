@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<div class="container">
<h1 style="text-align:center;">Редактирование категории</h1>
<hr>

<form class="form-horizontal" action="{{route('admin.category.update', $category)}}" method="post">
<input type="hidden" name="_method" value="PUT">
{{ csrf_field() }}

@include('admin.categories.part.form')

</form>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection