@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<h1 style="text-align:center;">Создание категорий</h1>
<div class="container" >

<a href="{{route('admin.category.create')}}" class="btn btn-primary pull-right">
    <i class="">Создать категорию</a>
    <table class="table table-striped">
        <thead>
            <th>Наименование</th>
            <th>Публикация</th>
            <th class="text-right">Действие</th>
</thead>
<tbody>
    @forelse ($categories as $category)
<tr>
    <td> 
        {{$category->title}}
    </td>
    <td> 
        {{$category->created_at}}
    </td>
    <td> 

    <div class="pull-right" style="">
          <a href="{{route('admin.category.edit', $category)}}" class="btn btn-primary">Редактирование</a>
     </div>
     @if(auth()->user()->role_id == 2)
    <form onsubmit="if (confirm ('Удалить?') ) 
    { return true } 
    else 
    {return false}
    " action="{{route('admin.category.destroy', $category)}}" method="post">
    <input type="hidden" name="_method" value="DELETE">
    {{ csrf_field() }}

    <div class="pull-right" style="padding-right:10px;">
          <button type="submit" class="btn btn-primary">Удаление</button>
     </div>


    </form>
     
     @endif
    </td>
</tr>
    @empty
<tr>
    <td colspan="3" class="text-center">
<h1>
    Данные отсутствуют
</h1>
</td>
</tr>
    @endforelse
</tbody>

<tfoot>
<tr>
<td colspan="3" class="text-center">
<ul class="pagination pull-center">
{{$categories->links()}}
</ul>
</td>
</tr>
</tfoot>
</table>
</a>

<div class="pull-left" style="padding-right:20px">
                <a href="/admin" class="btn btn-primary">Назад</a>
            </div>

</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection