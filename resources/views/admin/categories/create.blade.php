@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<h1 style="text-align:center;">Создание категории</h1>
<div class="container">

<hr>

<form class="form-horizontal" action="{{route('admin.category.store')}}" method="post">
{{ csrf_field() }}

    @include('admin.categories.part.form')

</form>
</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection
