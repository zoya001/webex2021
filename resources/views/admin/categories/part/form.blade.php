<label for="">Наименование</label>
<input type="text" class="form-control" name="title" placeholder="Заголовок категории" value="{{$category->title or ""}}" required>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Автоматическая генерация" value="{{$category->slug or ""}}" readonly="">

<hr>

<input class="btn btn-primary" type="submit" value="Сохранить">

<div class="pull-left" style="padding-right:20px">
                <a href="{{route('admin.category.store')}}" class="btn btn-primary">Назад</a>
            </div>
</div>