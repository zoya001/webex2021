@extends('admin.layouts.app_admin')

@section('content')
@if((auth()->user()->role_id == 2) or (auth()->user()->role_id == 3))
<!doctype html>
<html lang="ru">
  <head>
 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Пример на bootstrap 4: Макет jumbotron с навигационной панели и базовая система разметки. Версия v4.4.1.">

    <title>Блог</title>

    <!-- Bootstrap core CSS -->
<link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
    
    body{
      background:#fcfcfc;
    }

      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      
  .adminka{
    padding:10px;
    margin:10px;
    text-align:center;
  }

  </style>

    <link href="jumbotron.css" rel="stylesheet">
  </head>
  <body>

  <section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-12 col-md-8 mx-auto">
        <h1 class="fw-light">Административная панель</h1>
        <p class="lead text-muted">Создание, удаление, редактирование категорий, статей, пользователей. Доступна только зарегистрированным пользователям.</p>
        <p>
          <a href="/" class="btn btn-secondary my-2">На главную</a>
        </p>
      </div>
    </div>
  </section>

<main role="main">
  <div class="container">
    <div class="adminka">
    <div class="row">
      <div class="col-md-4">
        <h2> Создание категорий для статей</h2>
        <p>Создание, редактирование и удаление категорий для статей для быстрого поиска материалов. Указываются название категории.</p>
        <p><a class="btn btn-primary my-2" href="{{route('admin.category.store')}}" role="button"> Перейти к созданию &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Добавление и написание материалов</h2>
        <p>Написание материалов, содержащих в себе текст и изображение. Указываются категории и авторы статей.</p>
        <p><a class="btn btn-primary my-2" href="{{route('admin.article.store')}}" role="button">Создать материал &raquo;</a></p>
      </div>
      <div class="col-md-4">
        <h2>Создание пользователей</h2>
        <p>Добавление, редактирование и удаление пользователей администратором сайта. Доступно только для администратора.</p>
        <p><a class="btn btn-primary my-2" href="{{route('admin.author.store')}}" role="button">Перейти к пользователям &raquo;</a></p>
      </div>
    </div>
</div>
</div>
@else
<div class="container" >

<h3>У вас недостаточно прав на просмотр данной страницы</h3>
<div class="pull-left" style="padding-right:20px">
                <a href="/" class="btn btn-primary">На главную</a>
            </div>

</div>
@endif
@endsection
